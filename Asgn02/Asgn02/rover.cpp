#include "rover.h"
#include <iostream>
using namespace std;

scandata sc;

Rover::Rover()
{
	myQ = new Queue();
	myStack = new Stack();
}

Rover::Rover(int iNum, int resMax)
{
	myQ = new Queue();
	myStack = new Stack();
	ID = iNum;
	max = resMax;
}

Rover::~Rover()
{

}

void Rover::deploy() // this deploys a rover on a new mission
{
	printID();
	cout << " deploying..." << endl;
	printID();
	cout << " ready." << endl;
}

void Rover::move(int x, int y) // this moves a rover to the new x,y coordinates
{
//	myStack.Push(x, y);
	cout << printID() << "moving to location";
	cout << x << ", " << y << "." << endl;
}

void Rover::corescan() // this performs a core sample scan and stores the resuls
{
//	int x;
//	int xC = myStack.getCurX();
//	int yC = myStack.getCurY();
	cout << printID() << "scanning." << endl;
//	x = sc.getScandata(xC, yC);
//	myQ.Enqueue(x);
}

void Rover::dock() // this method returns to the base station and reports the result
{
	cout << printID() << "returning to base." << endl;
//	myStack.BackTrack(getID());
	cout << printID() << " at base. Sending results..." << endl;
//	myQ.SendRes(getID());
	cout << printID() << "result transmission complete." << endl;
	cout << printID() << "docked." << endl;
}

string Rover::printID()
{
	string myRover;
	myRover += "Rover (ID ";
	myRover += getID();
	myRover += ")";
	return myRover;
}
int Rover::getID()
{
	return ID;
}