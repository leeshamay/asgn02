// RETURN INSTRUCTIONS //
// LAST IN FIRST OUT //

#include "stack.h"
#include <iostream>
using namespace std;

Stack::Stack() 
{
	top = 0;
}

void Stack::Push(int x, int y)
{
	Node* NewNode = new Node();
	NewNode->xLoc = x;
	NewNode->yLoc = y;
	NewNode->next = top;
	top = NewNode;
}

void Stack::Pop()
{
	Node* temp = top;
	if (top == 0)
		return;
	top = top->next;
	delete (temp);
}

int Stack::IsEmpty()
{
	if (top == 0)
		return 1;
	return 0;
}

void Stack::BackTrack(int id) // Traverse the queue from the tail
{
	while(top->next != 0)
	{
		cout << "Rover (ID " << id << ")";
		cout << "moving to location";
		cout << top->xLoc << ", " << top->yLoc << "." << endl;
		Pop();
	}
}

int Stack::getCurX()
{
	return top->xLoc;
}
int Stack::getCurY()
{
	return top->yLoc;
}

Stack::~Stack()
{

}