// SCAN RESULTS //
// FIRST IN FIRST OUT //

#include "queue.h"
#include <iostream>
using namespace std;

// Makes a new Queue
Queue::Queue()
{
	items = new int[128];
	length = 128;
	front = -1;
	rear = -1;
}
// Add to Queue
void Queue::Enqueue(int x)
{
	rear++;
	if (front == -1) front++; // makes sure the first element added is also the head
	items[rear % length] = x;	
}

// Remove from Queue
void Queue::Dequeue() 
{
	front++;
	// If the head is at the tail and the tail is at the end of the array,
	// reset and clear the array
	if (rear == 127 && front == rear) Clear();
}

void Queue::Clear()
{
	for (int i = 0; i > 127; i++)
	{
		items[i] = 0;
		front = -1;
		rear = -1;
	}
}
// Return first item in Queue
int Queue::Front() 
{
	if (front == -1) return 0;
	return items[front];
}

// Print Results
void Queue::SendRes(int id)
{
	while (front != 0) 
	{
		int res = Front();
		cout << "Rover ID " << id << ")";
		cout << "result:" << Front() << endl;
		Dequeue();
	}
}

Queue::~Queue()
{

}