// SCAN RESULTS //
// FIRST IN FIRST OUT //
#pragma once
class Queue
{
	public:
		// Makes a new Queue
		Queue();
		// Destructor
		~Queue();
		// Add to Queue
		void Enqueue(int x);
		// Send Results
		void SendRes(int id);
	private:
		// Front/Rear 
		int front;
		int rear;

		int length; // Length
		int* items; // Storage
		// Return first in queue
		int Front();
		// Remove first item from Queue
		void Dequeue();
		// Clear and reset the array
		void Clear();
};