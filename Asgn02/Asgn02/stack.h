// RETURN INSTRUCTIONS //
// LAST IN FIRST OUT //
#pragma once
class Stack
{
	struct Node
	{
		int xLoc;
		int yLoc;
		Node* next;
	}; // try to put this outside later??

    private:
		// Global Top Variable
		Node* top;
    public:
		void BackTrack(int id); // Traverse the queue from the tail
		Stack();
		~Stack();
		void Push(int x, int y);
		void Pop();
		int IsEmpty();
		int getCurX();
		int getCurY();
};