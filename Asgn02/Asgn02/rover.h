#pragma once

#include <iostream>
#include <string>
#include "stack.h"
#include "queue.h"
#include "scandata.h"

class Rover
{
private:
	// Variables
	Stack* myStack;
	Queue* myQ;
	int ID;
	int max;

	// Methods
	std::string printID();
	int getID();

public:
	// Methods
	Rover();
	~Rover();
	Rover(int iNum, int resMax);
	void deploy(); // this deploys a rover on a new mission
	void move(int x, int y); // this moves a rover to the new x,y coordinates
	void corescan(); // this performs a core sample scan and stores the resuls
	void dock(); // this method returns to the base station and reports the result
};